# frozen_string_literal: true

require_relative "endtoendpaper_palindrome/version"

module EndtoendpaperPalindrome
# Returns true for a palindrome, false otherwise.
  def palindrome?
    processed_content == processed_content.reverse
  end

  private
  # Returns content for palindrome testing.
  def processed_content
    to_s.scan(/[a-z\d]/i).join.downcase
  end
end

class String
  include EndtoendpaperPalindrome
end

class Integer
  include EndtoendpaperPalindrome
end
